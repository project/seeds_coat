# Seeds Coat

Seeds Coat supports the initiative to fully support Drupal for the RTL
interface. We exploit all our UX flaws in training sessions to create
user-friendly and clean interfaces for websites based on Layout Builder.

## Table of Contents

- Introduction 
- Installation
- Maintainers

## Introduction

Seeds Coat built for developers and could be used for the following reasons:

Accelerate themeing:
We harnessed Gulp to install all needed libraries and auto compile your SCSS
files during development. No need to consume any second for your RTL interfaces
as its auto-generated from Gulp.

Layout builder enhancements:
Seeds Coat idea was to make continuum development on layout builder experience
and ability to share all new enhancement for all of our clients, we have very
good feedback from end-users and this journey will not stop, use Seeds Coat to
enjoy the essence of our deep thinking on how to make it more friendly.

New tabs experience:
A little code added for that, but it makes a huge impact on the user's
experience, 9 of 10 users who used the new experience like it and found it
more clear.

## Installation

To install Seeds Coat, copy seeds_coat into the root-level themes directory,
or into a themes directory, as seeds_coat.

Once installed, you can visit /admin/appearance/settings/seeds_coat to
configure the theme.

## Maintainers

- Mohammad Abdul-Qader - [m.abdulqader](https://www.drupal.org/u/mabdulqader)
- Yahya Al Hamad - [YahyaAlHamad](https://www.drupal.org/u/yahyaalhamad)
- Ahmad Alyasaki - [Ahmad Alyasaki](https://www.drupal.org/u/ahmad-alyasaki)
