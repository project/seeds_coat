/**
 * @file
 * Seeds Coat fixes.
 *
 */
(function ($, Drupal, once) {
  "use strict";

  Drupal.behaviors.seedsCoat = {
    attach: function (context, settings) {
      // Node edit tab.
      $(once("seedsCoat", ".tabs")).click(function () {
        $(this)
          .children(".nav-tabs.primary")
          .slideToggle("slow", function () {});
      });
      // Remove on click when clicking on contextual links.
      $(window).on("load", function () {
        $("*[onclick*='location.href'] div.contextual").on(
          "click",
          function (event) {
            event.stopPropagation();
          }
        );
        // Add class to skip dialog style if you are in layout builder
        if ($("#layout-builder").length !== 0) {
          $("body").addClass("layout-builder-on");
        }
      });
    },
  };
})(jQuery, Drupal, once);
